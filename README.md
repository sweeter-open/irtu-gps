# iRTU-GPS服务端

## 项目介绍

基于[iRTU](https://gitee.com/hotdll/iRTU)项目,实现GPS数据的接收和展示

非常感谢张涛(网名稀饭放姜)做了如此优秀的iRTU固件

服务器信息: gps.nutz.cn 端口 19002 前端页面地址是 http://gps.nutz.cn/ 预编译包可以"发行版"页面找到

技术特点:
1. 基于smart-socket的高性能io框架,轻松支持10w设备同时在线
2. 直接支持iRTU内置的JSON注册包
3. 无缝支持GPS的json数据包和hex数据包, 推荐选用hex,节省流量,性能更高
4. 自动建表,自动初始化. 默认配置H2数据库, 无需额外安装. 可按需要使用其他数据库
5. 基于NutzBoot,单个jar即可运行, 无需web容器,内存占用最低可到48mb
6. 配套微信小程序 https://gitee.com/wendal/irtu-gps-miniapp

![](miniapp.jpg)

![](air800_irtu_gps.jpg)

## iRTU配置实例

* 所需硬件: Air800 M4开发板, 或 A9532L车载GPS定位器. 前者在 http://m.openluat.com 有售, 后者开源可自行打样,PCB文件可在项目附件找到.
* 工具软件: Luatools最新版, 可到 http://www.openluat.com 的产品中心下载
* 底层软件: iRTU 1.8.7 https://github.com/hotdll/iRTU/releases
* 刷机教程: http://ask.openluat.com/article/4
* QQ支持群: 合宙Luat(稀饭放姜iRTU) 952343033

使用上述材料刷机为iRTU固件后,请联系"稀饭放姜",把imei和账号发给他, 将设备加入到你的账号下.

配置页面 http://dtu.openluat.com 账号名为手机号码,密码默认是888888

### 配置注册包为JSON

建议新建一个分组来存放新的配置信息.

json注册包才包含imei, 用于服务器识别设备. 
![](iRTU_config_1.jpg)

### 配置串口1和串口2,均为115200 8N1

串口常规设置
![](iRTU_config_2.jpg)

![](iRTU_config_3.jpg)

### 配置网络通道

配置为demo服务器的地址,
![](iRTU_config_4.jpg)

### 配置GPS参数

![](iRTU_config_5.jpg)

### 重启设备并访问前端页面

* 记得把设备的分组改成上面的配置分组.
* 重启设备(断电重启或者复位)后, 设备会获取到信息的配置信息, 然后再次重启
* 待设备再次启动完成后, 网络灯慢闪, 打开前端页面(例如demo页面 http://gps.nutz.cn ),输入imei, 若定位成功, 即可看到设备的当前位置.
* 提醒: 室内无GPS信号,请到室外空旷处.

## 开发须知

本压缩包是一个maven工程, eclipse/idea均可按maven项目导入

MainLauncher是入口,右键直接启动即可

### 环境要求

* 必须JDK8+
* eclipse或idea等IDE开发工具,可选

### 配置信息位置

数据库配置信息,jetty端口等配置信息,均位于src/main/resources/application.properties

### 命令下启动

仅供测试用,使用mvn命令即可

```
// for windows
set MAVEN_OPTS="-Dfile.encoding=UTF-8"
mvn compile nutzboot:run

// for *uix
export MAVEN_OPTS="-Dfile.encoding=UTF-8"
mvn compile nutzboot:run
```

### 项目打包

```
mvn clean package nutzboot:shade
```

请注意,当前需要package + nutzboot:shade, 单独执行package或者nutzboot:shade是不行的


### 跳过测试
```
mvn clean package nutzboot:shade -Dmaven.test.skip=true
```

## 相关资源

* 论坛: https://nutz.cn 如有任何疑问,请发帖
* 官网: https://nutz.io
* 一键生成NB的项目: https://get.nutz.io
