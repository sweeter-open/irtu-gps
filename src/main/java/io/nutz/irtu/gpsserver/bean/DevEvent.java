package io.nutz.irtu.gpsserver.bean;

import org.nutz.dao.entity.annotation.Index;
import org.nutz.dao.entity.annotation.Name;
import org.nutz.dao.entity.annotation.Table;
import org.nutz.dao.entity.annotation.TableIndexes;
import org.nutz.dao.interceptor.annotation.PrevInsert;

@Table("t_dev_event")
@TableIndexes({@Index(fields= {"imei", "event", "srvtime"}, unique=false)})
public class DevEvent extends AbstractDevMsg {

    @Name
    @PrevInsert(uu32=true)
    protected String id;
    protected String devtime;
    protected String event;
    protected boolean readed;
}
