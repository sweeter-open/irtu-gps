package io.nutz.irtu.gpsserver.bean;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.List;

import org.nutz.dao.entity.annotation.Name;
import org.nutz.dao.entity.annotation.Table;

@Table("dev_status")
public class DevStatusMsg extends AbstractDevMsg {

    @Name
    protected String imei;

    // {"isopen", "vib", "acc", "act", "chg", "und", "volt", "vbat", "csq"}
    protected boolean gpsopen;
    protected boolean vib;
    protected boolean acc;
    protected boolean act;
    protected boolean chg;
    protected boolean und;
    protected int volt;
    protected int vbat;
    protected int csq;
    
    public static final DevStatusMsg from(List<Object> list) {
        Object[] params = list.toArray(new Object[list.size()]);
        DevStatusMsg msg = new DevStatusMsg();
        msg.gpsopen = ((Boolean)params[0]).booleanValue();
        msg.vib = (Boolean)params[1];
        msg.acc = (Boolean)params[2];
        msg.act = (Boolean)params[3];
        msg.chg = (Boolean)params[4];
        msg.und = (Boolean)params[5];
        msg.volt = ((Number)params[6]).intValue();
        msg.vbat = ((Number)params[7]).intValue();
        msg.csq = ((Number)params[8]).intValue();
        msg.srvtime = System.currentTimeMillis();
        return msg;
    }
    public static DevStatusMsg from(byte[] buf) {
        try {
            DevStatusMsg msg = new DevStatusMsg();
            DataInputStream dis = new DataInputStream(new ByteArrayInputStream(buf));
            dis.readUnsignedByte(); // skip header
            msg.gpsopen = dis.readBoolean();
            msg.vib = dis.readBoolean();
            msg.acc = dis.readBoolean();
            msg.act = dis.readBoolean();
            msg.chg = dis.readBoolean();
            msg.und = dis.readBoolean();
            msg.volt = dis.readUnsignedShort() * 256 * 256 + dis.readUnsignedShort();
            msg.vbat = dis.readUnsignedShort();
            msg.csq = dis.readUnsignedByte();
            msg.srvtime = System.currentTimeMillis();
            return msg;
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public boolean isGpsopen() {
        return gpsopen;
    }
    public void setGpsopen(boolean gpsopen) {
        this.gpsopen = gpsopen;
    }
    public boolean isVib() {
        return vib;
    }
    public void setVib(boolean vib) {
        this.vib = vib;
    }
    public boolean isAcc() {
        return acc;
    }
    public void setAcc(boolean acc) {
        this.acc = acc;
    }
    public boolean isAct() {
        return act;
    }
    public void setAct(boolean act) {
        this.act = act;
    }
    public boolean isChg() {
        return chg;
    }
    public void setChg(boolean chg) {
        this.chg = chg;
    }
    public boolean isUnd() {
        return und;
    }
    public void setUnd(boolean und) {
        this.und = und;
    }
    public int getVolt() {
        return volt;
    }
    public void setVolt(int volt) {
        this.volt = volt;
    }
    public int getVbat() {
        return vbat;
    }
    public void setVbat(int vbat) {
        this.vbat = vbat;
    }
    public int getCsq() {
        return csq;
    }
    public void setCsq(int csq) {
        this.csq = csq;
    }
    
    
}
